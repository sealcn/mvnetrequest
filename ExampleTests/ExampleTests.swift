//
//  ExampleTests.swift
//  ExampleTests
//
//  Created by RentonLIn on 2018/11/21.
//  Copyright © 2018 Renton. All rights reserved.
//

import XCTest
@testable import Example
import MVNetRequest
import Alamofire
import SwiftyJSON
import RxSwift

class ExampleTests: XCTestCase {
    
    let disposeBag = DisposeBag()
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testUrl() {
        let request = MVNetRequest(host: "https://httpbin.org/", url: "", parameters: nil,
                                   method: HTTPMethod.get, encodingType: URLEncoding.default)
        let url = request.getRequretUrl()
        XCTAssert(url == "https://httpbin.org/")
    }
    
    func testUrlPath() {
        let request = MVNetRequest(host: "https://httpbin.org/",
                                   url: "haha",
                                   parameters: nil,
                                   method: HTTPMethod.get,
                                   encodingType: URLEncoding.default,
                                   urlPath: "peace/v1/", headers: [:],
                                   defaultParsePath: nil)
        let url = request.getRequretUrl()
        XCTAssert(url == "https://httpbin.org/peace/v1/haha")
    }
    
    func testGetAndParameter() {
        let expectation = XCTestExpectation(description: "\(#function)")
        let request = MVNetRequest(host: "https://httpbin.org/get", url: "", parameters: ["key1" : "value1", "key2" : "value2"],
                                   method: HTTPMethod.get, encodingType: URLEncoding.default)
        let _ = request.connect().subscribe(onNext: { (result) in
            guard let result = result as? JSON else {
                XCTFail()
                return
            }
            XCTAssert(result["args"]["key1"].stringValue == "value1")
            XCTAssert(result["args"]["key2"].stringValue == "value2")
            expectation.fulfill()
        }, onError: { (error) in
            XCTFail()
        }, onCompleted: nil, onDisposed: nil).disposed(by: disposeBag)
        wait(for: [expectation], timeout: 10)
    }
    
    func testPostAndParameter() {
        let expectation = XCTestExpectation(description: "\(#function)")
        let request = MVNetRequest(host: "https://httpbin.org/post", url: "", parameters: ["key1" : "value1", "key2" : "value2"],
                                   method: HTTPMethod.post, encodingType: JSONEncoding.default)
        let _ = request.connect().subscribe(onNext: { (result) in
            guard let result = result as? JSON else {
                XCTFail()
                return
            }
            XCTAssert(result["json"]["key1"].stringValue == "value1")
            XCTAssert(result["json"]["key2"].stringValue == "value2")
            expectation.fulfill()
            
        }, onError: { (error) in
            XCTFail()
        }, onCompleted: nil, onDisposed: nil).disposed(by: disposeBag)
        wait(for: [expectation], timeout: 10)
    }
    
    func testHeader() {
        let expectation = XCTestExpectation(description: "\(#function)")
        let request = MVNetRequest(host: "https://httpbin.org/get", url: "", parameters: ["key1" : "value1", "key2" : "value2"],
                                   method: HTTPMethod.get, encodingType: URLEncoding.default,
                                   headers: ["Xcodeversion" : "10.0", "Devicetest" : "iPhone"])
        let _ = request.connect().subscribe(onNext: { (result) in
            guard let result = result as? JSON else {
                XCTFail()
                return
            }
            XCTAssert(result["headers"]["Xcodeversion"].stringValue == "10.0")
            XCTAssert(result["headers"]["Devicetest"].stringValue == "iPhone")
            expectation.fulfill()
        }, onError: { (error) in
            XCTFail()
        }, onCompleted: nil, onDisposed: nil).disposed(by: disposeBag)
        wait(for: [expectation], timeout: 10)
    }
    
    func testParsePathResult() {
        let expectation = XCTestExpectation(description: "\(#function)")
        let request = MVNetRequest(host: "https://httpbin.org/get", url: "", parameters: ["key1" : "value1", "key2" : "value2"],
                                   method: HTTPMethod.get, encodingType: URLEncoding.default,
                                   defaultParsePath: "args")
        let _ = request.connect().subscribe(onNext: { (result) in
            guard let result = result as? JSON else {
                XCTFail()
                return
            }
            XCTAssert(result["key1"].stringValue == "value1")
            XCTAssert(result["key2"].stringValue == "value2")
            expectation.fulfill()
        }, onError: { (error) in
            XCTFail()
        }, onCompleted: nil, onDisposed: nil).disposed(by: disposeBag)
        wait(for: [expectation], timeout: 10)
    }
    
    func testCache() {
        let expectation = XCTestExpectation(description: "\(#function)")
        var count = 0
        let request = MVNetRequest(host: "https://httpbin.org/get", url: "", parameters: ["key1" : "value1", "key2" : "value2"],
                                   method: HTTPMethod.get, encodingType: URLEncoding.default)
        request.useCache = true
        let _ = request.connect().take(1).subscribe(onNext: { _ in
            request.connect().subscribe(onNext: { (result) in
                guard let result = result as? JSON else {
                    XCTFail()
                    return
                }
                XCTAssert(result["args"]["key1"].stringValue == "value1")
                XCTAssert(result["args"]["key2"].stringValue == "value2")
                count += 1
            }, onError: { (error) in
                XCTFail()
            }, onCompleted: {
                XCTAssert(count == 2)
                expectation.fulfill()
            }, onDisposed: nil).disposed(by: self.disposeBag)
        }).disposed(by: disposeBag)
        wait(for: [expectation], timeout: 10)
    }
}

