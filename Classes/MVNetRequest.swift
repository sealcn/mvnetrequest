//
//  MVNetRequest.swift
//  NetRequest
//
//  Created by RentonLIn on 2018/11/21.
//  Copyright © 2018 Renton. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire
import RxSwift
import Cache

public typealias RequestCompleteClosure = () -> Void

//MARK: Cache
fileprivate let requestStorga = try? Storage<String, Data> (diskConfig: DiskConfig(name: "RequestCache"),
                                              memoryConfig: MemoryConfig(expiry: .never, countLimit: 100, totalCostLimit: 100),
                                              transformer: TransformerFactory.forData())

//MARK: Request

public let User401NotificationName = "notification.request.401"

open class MVNetRequest: NSObject {
    
    private var host: String
    private var url: String
    private var parameters: [String: Any]?
    private var method: Alamofire.HTTPMethod = .get
    private var encodingType: ParameterEncoding = URLEncoding.default
    public var useCache: Bool = false
    public var cacheKey: String? = nil
    ///如果有cache，不触发网络错误，仅针对调用connect()方法
    public var preferCache: Bool = false
    private var urlPath: String = ""
    private var headers: [String : String] = [:]
    private var defaultParsePath: String? = nil
    
    var downloadRequest: DownloadRequest!
    
    fileprivate var alamofireRequest: Alamofire.DataRequest?
    
    public init(host: String,
                url: String,
                parameters: [String: Any]?,
                method: Alamofire.HTTPMethod,
                encodingType: ParameterEncoding,
                urlPath: String = "",
                headers: [String: String] = [:],
                defaultParsePath: String? = nil) {
        self.host = host
        self.url = url
        self.parameters = parameters
        self.method = method
        self.encodingType = encodingType
        self.urlPath = urlPath
        self.headers = headers
        self.defaultParsePath = defaultParsePath
    }
    
    open func parseData(data: JSON?) -> Any? {
        return data
    }
    
    private var requestUrl: String {
        get {
            return host + self.urlPath + url
        }
    }
    
    public func getRequretUrl() -> String {
        return requestUrl
    }
    
    private func send(extraHeaders: [String : String] = [:],
                      success: @escaping (_ data: Any?) -> Void,
                      complete: RequestCompleteClosure? = nil,
                      failure: @escaping (_ error: Error) -> Void)
    {
        let  headers = self.headers
        
        //FIXME: Renton 用URL的方式去拼，解决子类不知道该不该加/的问题
        print("请求URL：\(requestUrl), parameters:\(String(describing: parameters))")
        
        if method == .get && useCache {
            do {
                if let cachedData = try requestStorga?.object(forKey: cacheKey ?? requestUrl) {
                    processData(data: cachedData, success: success, complete: complete, failure: failure)
                }
            } catch {
                //                print("fail to load data")
            }
        }
        
        let request = AF.request(requestUrl, method: method, parameters: parameters, encoding: self.encodingType, headers: httpHeaders(from: headers)).responseString { response in
            if let error = response.error {
                failure(error)
                return
            }
            if let statusCode = response.response?.statusCode, statusCode >= 400 {
                switch statusCode {
                case 401:
                    NotificationCenter.default.post(
                        name: NSNotification.Name(rawValue: User401NotificationName),
                        object: nil,
                        userInfo: nil)
                    failure(NotLoginError())
                case 404:
                    failure(MVCustomError(description: "\(statusCode) Fail to locate the resource"))
                case 400:
                    failure(MVCustomError(description: "\(statusCode) Invaid parameter"))
                default:
                    failure(MVCustomError(description: "\(statusCode) Unkown error happened"))
                }
                return
            }
            
            guard let data = response.data else {
                failure(NoDataError())
                return
            }
            self.processData(data: data, success: success, complete: complete, failure: failure, needSaveToCached: true, needFinishRequest: true)
        }
        
        self.alamofireRequest = request
        
    }
    
    open func processData(data: Data,
                          success: @escaping (_ data: Any?) -> Void,
                          complete: RequestCompleteClosure?,
                          failure: @escaping (_ error: Error) -> Void,
                          needSaveToCached: Bool = false,
                          needFinishRequest: Bool = false) {
        do {//po String(data: data, encoding: .utf8)
            let json = try JSON(data: data)
            if useCache && needSaveToCached {
                do {
                    try requestStorga?.setObject(data, forKey: cacheKey ?? requestUrl)
                } catch {
                    #if DEBUG
                    print("save to cache failed")
                    #endif
                }
            }
            
            var parseJson = json
            if let path = defaultParsePath {
                parseJson = json[path]
            }
            success(self.parseData(data: parseJson))
            if needFinishRequest {
                complete?()
            }
        } catch {
            failure(NoDataError())
            return
        }
    }
    
    //暂时没人用，先隐藏
    private func cancel() {
        self.alamofireRequest?.cancel()
    }
    
    public func connect() -> Observable<Any?> {
        return Observable<Any?>.create({ (observer) -> Disposable in
            let headers = self.headers
            let requestUrl = self.requestUrl
            let method = self.method
            let parameters = self.parameters
            let encodingType = self.encodingType
            
            let useCache = self.useCache
            let cacheKey = self.cacheKey
            //FIXME: Renton 用URL的方式去拼，解决子类不知道该不该加/的问题
            print("请求URL：\(requestUrl), parameters:\(String(describing: parameters))")
            
            var hadCachedData: Bool = false
            let preferCache = self.preferCache
            if method == .get && useCache {
                if let storage = requestStorga,
                    let cachedData = try? storage.object(forKey: cacheKey ?? requestUrl),
                    let json = try? JSON(data: cachedData){
                    var parsedJson = json
                    if let parseParth = self.defaultParsePath {
                        parsedJson = json[parseParth]
                    }
                    hadCachedData = true
                    observer.onNext(self.parseData(data: parsedJson))
                }
            }
            
            let request = AF.request(requestUrl, method: method, parameters: parameters, encoding: encodingType, headers: self.httpHeaders(from: headers)).responseString { response in
                if let error = response.error {
                    if hadCachedData && preferCache {
                        return
                    } else {
                        //FIXME: Renton error 需要带来原始的返回信息
                        observer.onError(error)
                        return
                    }
                }
                
                if let statusCode = response.response?.statusCode, statusCode >= 400 {
                    switch statusCode {
                    case 401:
                        NotificationCenter.default.post(
                            name: NSNotification.Name(rawValue: User401NotificationName),
                            object: nil,
                            userInfo: nil)
                        observer.onError(NotLoginError())
                    case 404:
                        observer.onError(MVCustomError(description: "\(statusCode) Fail to locate the resource"))
                    case 400:
                        observer.onError(MVCustomError(description: "\(statusCode) Invaid parameter"))
                    default:
                        observer.onError(MVCustomError(description: "\(statusCode) Unkown error happened"))
                    }
                    return
                }
                
                guard let data = response.data,
                let json = try? JSON(data: data) else {
                    observer.onError(NoDataError())
                    return
                }
                
                if let error = self.onReceiveResponse(json: json) {
                    observer.onError(error)
                    return
                }
                
                if method == .get && useCache {
                    try? requestStorga?.setObject(data, forKey: cacheKey ?? requestUrl)
                }
                var parseJson = json
                if let path = self.defaultParsePath {
                    parseJson = json[path]
                }
                observer.onNext(self.parseData(data: parseJson))
                observer.onCompleted()
            }
            
            self.alamofireRequest = request
            return Disposables.create {
                //TODO:  Renton  release request
            }
        })
    }
    
    ///Override point
    open func onReceiveResponse(json: JSON) -> Error? {
        return nil
    }
    
    private func httpHeaders(from headers: [String: String]) -> HTTPHeaders {
        var httpHeaders = HTTPHeaders()
        headers.forEach { httpHeaders.update(name: $0.key, value: $0.value) }
        return httpHeaders
    }
    
}

// MARK: - errors
public class ServerError: Error {
    public var code: Int
    public var message: String?
    
    public init(code: Int, message: String?) {
        self.code = code
        self.message = message
    }
}

open class ParameterError: Error, LocalizedError {
    public init() {
    }
    
    open var errorDescription: String? {
        get {
            return "Something is wrong with input value"
        }
    }
}

open class NoDataError: Error, LocalizedError {
    public init() {
    }
    
    open var errorDescription: String? {
        get {
            return "No data is found"
        }
    }
}

open class NotLoginError: Error, LocalizedError {
    public init() {
    }
    
    open var errorDescription: String? {
        get {
            return "Need login first"
        }
    }
}

open class MVCustomError: Error, LocalizedError {
    private var errorMessage: String
    public init(description: String) {
        errorMessage = description
    }
    
    public var errorDescription: String? {
        get {
            return errorMessage
        }
    }
}
