//
//  RootRequest.swift
//  MVNetRequest
//
//  Created by rentonlin on 12/25/18.
//  Copyright © 2018 Renton. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

public class RootRequestConfig {
    public static var host: String?
    public static var urlPath: String?
    public static var headers: [String : String]?
}

//MARK: - RootRequest
open class RootRequest: MVNetRequest {
    ///配置的三个值写成!是故意，以提醒你设置对应的值
    public init(host: String = RootRequestConfig.host!,
                url: String,
                parameters: [String: Any]?,
                method: Alamofire.HTTPMethod = .get,
                encodingType: ParameterEncoding,
                urlPath: String = RootRequestConfig.urlPath!) {
        super.init(host: host,
                   url: url, parameters: parameters,
                   method: method,
                   encodingType: encodingType,
                   urlPath: urlPath,
                   headers: RootRequestConfig.headers!,
                   defaultParsePath: "data")
    }
    
    override open func processData(data: Data,
                                     success: @escaping (_ data: Any?) -> Void,
                                     complete: RequestCompleteClosure?,
                                     failure: @escaping (_ error: Error) -> Void,
                                     needSaveToCached: Bool = false,
                                     needFinishRequest: Bool = false) {
        do {
            let json = try JSON(data: data)
            guard let code = json["status"].dictionaryValue["code"]?.intValue else {
                failure(NoDataError())
                return
            }
            if code == 0 {
                super.processData(data: data,
                                  success: success,
                                  complete: complete,
                                  failure: failure,
                                  needSaveToCached: needSaveToCached,
                                  needFinishRequest: needFinishRequest)
            } else {
                failure(ServerError(code: code, message: json["status"]["message"].string))
            }
        } catch {
            failure(NoDataError())
            return
        }
    }
    
    override open func onReceiveResponse(json: JSON) -> Error? {
        guard let code = json["status"].dictionaryValue["code"]?.intValue else {
            return NoDataError()
        }
        guard code == 0 else {
            return ServerError(code: code, message: json["status"]["message"].string)
        }
        return nil
    }
}
