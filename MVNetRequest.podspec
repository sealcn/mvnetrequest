Pod::Spec.new do |s|
  s.name         = "MVNetRequest"
  s.version      = "0.0.9"
  s.summary      = "Request wrapper of Alamofire."

  s.description  = "MVNetRequest~~~~~~~~~~~~~~~~~~~~~~~"

  s.homepage     = "https://bitbucket.org/sealcn/mvnetrequest/"


  s.license      = "WTFPL"

  s.author             = { "Renton" => "weichao@dailyinnovation.biz" }

  s.platform     = :ios, "13.0"

  s.source       = { :git => "git@bitbucket.org:sealcn/mvnetrequest.git", :tag => "#{s.version}" }

  s.source_files  = "Classes"
#  s.exclude_files = "Classes/Exclude"

  s.dependency "Alamofire"
  s.dependency "RxSwift"
  s.dependency "SwiftyJSON"
  s.dependency "Cache"

end
