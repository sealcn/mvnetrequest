//
//  ViewController.swift
//  Example
//
//  Created by RentonLIn on 2018/11/21.
//  Copyright © 2018 Renton. All rights reserved.
//

import UIKit
import RxSwift
import Alamofire
import MVNetRequest

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        //get请求
//        let request = MVNetRequest(host: "https://httpbin.org/",
//                                   url: "get",
//                                   parameters: ["key1" : "value1",
//                                                "key2" : "value2"],
//                                   method: HTTPMethod.get,
//                                   encodingType: URLEncoding.default,
//                                   urlPath: "",
//                                   headers: ["Accept-Type" : "application/json"],
//                                   defaultParsePath: "headers")
//        request.useCache = true
//        request.cacheKey = "haha"
//        let _ = request.connect().subscribe(onNext: { (response) in
////            print(response ?? "nil")
//        }, onError: { (error) in
////            print(error)
//        }, onCompleted: nil, onDisposed: nil)
        
//        RootRequestConfig.host = "https://peace-cms-test.dailyinnovation.biz/"
//        let data = readData(fileName: "category_en")
//        RootRequestConfig.headers = ["Cookie" : "user=\"2|1:0|10:1545375741|4:user|32:NWFlM2Y0NDU0YzAxYWM0MTEyNDY4NmMx|19b7084a9e36a403dac3a1081bf9d91976d96c4bdd72eb165c9ca3d0b32a264f\""]
        
//        RootRequestConfig.host = "https://peace-cms-asian-test.dailyinnovation.biz/"
//        let data = readData(fileName: "category_zh")
//        RootRequestConfig.headers = ["Cookie" : "userid=\"2|1:0|10:1546402374|6:userid|24:MDIyNjE0NjQ1NjI2NTI4NDg4|92945c17c3d124f4adc4c7365dfb793ac848e20343105fafa9c176cb7ee70a31\"; name=\"2|1:0|10:1546402374|4:name|12:5p6X57u06LaF|21a426e91e19ad198670ca9229197d1c73f01a2190587c430029252c912230ab\"; uac_passport=\"2|1:0|10:1546402374|12:uac_passport|44:ZDNlZDI2ZDQ0MWRiNDllM2I5NTZlNzFmYzY5YTRhOGM=|409ee4b1904771420854885f36e3586336b473a9d39f16b08cb219a00b856bbc\"; avatar=\"2|1:0|10:1546402374|6:avatar|0:|b4f238e55645a88a768fad956968ea0b4c828e5fbe8db6514165a20bb530c430\""]
        
//        RootRequestConfig.host = "https://peace-cms.dailyinnovation.biz/"
//        let data = readData(fileName: "category_en")
//        RootRequestConfig.headers = ["Cookie" : "user=\"2|1:0|10:1545386804|4:user|32:NWFlM2Y0NDU0YzAxYWM0MTEyNDY4NmMx|c433424ccb4ad72500d8f947ca25cbb3e63910955b1e4b3430f9f1c311f2a772\""]
        
        RootRequestConfig.host = "https://peace-api.dailyinnovation.biz"
        //let data = readData(fileName: "category_zh")
        RootRequestConfig.headers = ["Cookie" : "user=\"2|1:0|10:1545386857|4:user|32:NWJkMTNmZTY1NDE2NDU2NTFiNjg5NmM4|42a0cd3cd188191a4472b8387f20924e27cafbf3a457428694520853c19d9434\""]
        
        RootRequestConfig.urlPath = "/peace/v1"
        
//        uploadCategory(data: data).subscribe(onNext: { (categories) in
//            print(categories)
//        }, onError: {error in
//            print(error)
//        }, onCompleted: {[weak self] in
//        }, onDisposed: nil)
        
//        uploadPackages(data: data)
        
        getHomeList()
    }
    
    private func getHomeList() {
        let request = RootRequest.init(url: "/home", parameters: nil, encodingType: URLEncoding.default)
        request.useCache = true
        request.cacheKey = "haha"
        request.preferCache = true
        let _ = request.connect().debug("fire").subscribe(onNext: { (response) in
            print(response ?? "nil")
        }, onError: { (error) in
            print("是否展示错误回调：\(error)")
        }, onCompleted: nil, onDisposed: nil)
    }
    
    private func readData(fileName: String) -> [(Category, [Package])] {
        var result : [String : [Package]] = [:]
        let filePathUrl = Bundle.main.url(forResource: fileName, withExtension: "csv")!
        let fileDataString = try! String(contentsOf: filePathUrl)
        
        var currentCategory: Category = Category()
        for line in fileDataString.components(separatedBy: "\r\n") {
            let parts = line.components(separatedBy: ",")
            guard parts.count == 3 else {
                continue
            }
            if !parts[0].isEmpty {
                currentCategory = Category()
                currentCategory.name = parts[0]
            }
            let package = Package()
            package.id = parts[2]
            
            if var packages = result[currentCategory.name] {
                packages.append(package)
                result[currentCategory.name] = packages
            } else {
                var packages: [Package] = []
                packages.append(package)
                result[currentCategory.name] = packages
            }
        }
        return result.map({ (key, value) -> (Category, [Package]) in
            let category = Category()
            category.name = key
            return (category, value)
        })
    }
    
    private func uploadCategory(data: [(Category, [Package])]) -> Observable<Any?> {
        let categories = data.map{$0.0}
        let ob = Observable.from(categories)
        let newOb = ob.concatMap { category -> Observable<Any?> in
            return UploadCategoryRequest(category: category).connect()
        }
        return newOb
    }
    
    private func uploadPackages(data: [(Category, [Package])]) {
        var dic = [String : [Package]]()
        for (category, packages) in data {
            dic[category.name] = packages
        }
        
        GetCategoriesRequest().connect().flatMap { (categories) -> Observable<Any?> in
            return Observable<Category>.from(categories as! [Category]).concatMap({ category -> Observable<Any?> in
                let packages = dic[category.name]!
                return UploadPackageRequest(category: category, packages: packages).connect()
            })
        }.subscribe()
    }

    //Post请求
//    let request = MVNetRequest(host: "https://httpbin.org/",
//                               url: "get",
//                               parameters: ["key1" : "value1",
//                                            "key2" : "value2"],
//                               method: HTTPMethod.get,
//                               encodingType: URLEncoding.default,
//                               urlPath: "",
//                               headers: ["Accept-Type" : "application/json"])
//    request.useCache = true
//    request.cacheKey = "haha"
//    let _ = request.connect().debug("fire").subscribe(onNext: { (response) in
//        print(response ?? "nil")
//    }, onError: { (error) in
//        print(error)
//    }, onCompleted: nil, onDisposed: nil)
}

