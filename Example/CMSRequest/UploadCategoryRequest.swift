//
//  UploadCategoryRequest.swift
//  Example
//
//  Created by rentonlin on 1/2/19.
//  Copyright © 2019 Renton. All rights reserved.
//

import Foundation
import MVNetRequest
import Alamofire
import SwiftyJSON

class Category {
    var name: String = "#CategoryName"
    var id: String = ""
    var color_bg: String = "#000000"
    var color_text: String = "#ff0000"
    var desc: String = "#what"
    var cate_image: String = "http://peace-lens.dailyinnovation.biz/category/87df8d06382d67b2c92621da9c4f894f.jpg"
}

class UploadCategoryRequest: RootRequest {
    init(category: Category) {
        let parameters: [String : Any] = ["color_bg" : category.color_bg,
                                          "color_text" : category.color_text,
                                          "cate_img" : category.cate_image,
                                          "name" : ["en" : category.name, "zh_Hans" : category.name, "zh_Hant" : category.name],
                                          "desc" : ["en" : category.desc, "zh_Hans" : category.desc, "zh_Hant" : category.desc]]
        super.init(url: "category", parameters: parameters,
                   method: .post, encodingType: JSONEncoding.default)
    }
}

class Package {
    var id: String = ""
}

class UploadPackageRequest: RootRequest {
    init(category: Category, packages: [Package]) {
        super.init(url: "category/\(category.id)/course", parameters: ["courseIds" : packages.map{$0.id}],
                   method: .put, encodingType: JSONEncoding.default)
    }
}

class GetCategoriesRequest: RootRequest {
    init() {
        super.init(url: "category", parameters: nil,
                   encodingType: JSONEncoding.default)
    }
    
    override func parseData(data: JSON?) -> Any? {
        guard let data = data else {
            return nil
        }
        return data["content"].arrayValue.map({ (json) -> Category in
            let category = Category()
            category.id = json["id"].stringValue
            category.name = json["name"]["en"].stringValue
            return category
        })
    }
}
